from django.urls import path
from receipts.views import (
    ExpenseCategoryCreate,
    ReceiptCreate,
    ReceiptListView,
    ExpenseCategoryView,
    AccountView,
    AccountCreate,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("accounts/", AccountView.as_view(), name="account_list"),
    path("accounts/create/", AccountCreate.as_view(), name="create_account"),
    path("create/", ReceiptCreate.as_view(), name="create_receipt"),
    path("categories/", ExpenseCategoryView.as_view(), name="list_categories"),
    path(
        "categories/create/",
        ExpenseCategoryCreate.as_view(),
        name="category_create",
    ),
]
