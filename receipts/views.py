from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin

from django.shortcuts import redirect

from receipts.models import Account, Receipt, ExpenseCategory


# Create your views here.
class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreate(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/create.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = self.request.user
        item.save()
        return redirect("home")

    def get_form(self, *args, **kwargs):
        form = super(ReceiptCreate, self).get_form(*args, **kwargs)
        form.fields["category"].queryset = ExpenseCategory.objects.filter(
            owner=self.request.user
        )
        form.fields["account"].queryset = Account.objects.filter(
            owner=self.request.user
        )
        return form


class ExpenseCategoryView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "expenses/list.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class ExpenseCategoryCreate(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "expenses/create.html"
    fields = ["name"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("list_categories")


class AccountView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "accounts/list.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class AccountCreate(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "accounts/create.html"
    fields = ["name", "number"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("account_list")
